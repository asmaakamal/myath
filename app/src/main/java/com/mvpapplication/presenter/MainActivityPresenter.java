package com.mvpapplication.presenter;

import com.mvpapplication.contract.MainActivityContract;
import com.mvpapplication.contract.MainActivityContract.Model;
import com.mvpapplication.contract.MainActivityContract.View;
import com.mvpapplication.model.MainActivityModel;

/**
 * Created by sagarkhakhar on 20/09/17.
 */

public class MainActivityPresenter implements MainActivityContract.Presenter {

  private View mView;
  private Model mModel;

  public MainActivityPresenter(View view) {
    mView = view;
    initPresenter();
  }

  private void initPresenter() {
    mModel = new MainActivityModel();
    mView.initView();
  }

  @Override
  public void onClick(android.view.View view) {
    String data = mModel.getData();
    mView.setViewData(data);
  }
}
